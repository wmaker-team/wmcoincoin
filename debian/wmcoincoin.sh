#! /bin/sh

test -x /usr/libexec/wmcoincoin/wmcoincoin || exit 1

if [ -z "$PATH" ]; then
  PATH=/usr/bin
fi
PATH=/usr/libexec/wmcoincoin:/usr/share/wmcoincoin:"$PATH"
export PATH

exec wmcoincoin "$@"
